"""
This module provides retrieving and storing functionality for the PSI Multi-Cultivator
"""

from pycultivator.config import baseConfig
from pycultivator.config.baseConfig import (
    CalibrationConfig,
    PolynomialConfig,
    SettingsConfig
)
from pycultivator_reglo.instrument import regloInstrument
from pycultivator_reglo import pump, regloPump
from pycultivator_reglo.connection import regloConnection

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class DeviceConfig(baseConfig.DeviceConfig):
    """Class to handle the configuration of pump object from/to configuration source"""

    _configures = regloPump.RegloPump
    _configures_types = {"device"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(DeviceConfig, cls).assert_source(source, expected)


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = regloConnection.RegloConnection

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ConnectionConfig, cls).assert_source(source, expected)


class ChannelConfig(baseConfig.ChannelConfig):
    """Config class for handling the configuration of a channel object from a configuration source"""

    _configures = regloPump.RegloPumpChannel

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ChannelConfig, cls).assert_source(source, expected)


class PumpConfig(baseConfig.InstrumentConfig):

    _configures = pump.Pump
    _configures_types = {"pump"}

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(PumpConfig, cls).assert_source(source, expected)


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    _namespace = "config"
    # set available helpers
    _known_helpers = [
        # baseConfig.ObjectConfig,
        # baseConfig.PartConfig,
        DeviceConfig,
        ConnectionConfig,
        ChannelConfig,
        PumpConfig,
        CalibrationConfig,
        PolynomialConfig,
        SettingsConfig
    ]

    default_settings = baseConfig.Config.mergeDefaultSettings(
        {
            # set extra settings
        }, namespace=_namespace
    )

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)


class ConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
