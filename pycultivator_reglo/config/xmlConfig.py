"""This module provides an API for serializing the configuration setting of the Multi-Cultivator"""

from pycultivator_reglo.config import baseConfig
from pycultivator.config import xmlConfig as baseXMLConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLDeviceConfig(baseXMLConfig.XMLDeviceConfig, baseConfig.DeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    XML_ELEMENT_TAG = "device"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLDeviceConfig, cls).assert_source(source, expected)


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a Multi-Cultivator connection object from XML"""

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLConnectionConfig, cls).assert_source(source, expected)

    def read_properties(self, definition, obj):
        """Read properties into the connection object

        :type definition: lxml.etree._Element._Element
        :type obj: pycultivator_reglo.connection.regloConnection.RegloConnection
        :rtype: pycultivator_reglo.connection.regloConnection.RegloConnection
        """
        obj = super(XMLConnectionConfig, self).read_properties(definition, obj)
        """:type: pycultivator_reglo.connection.regloConnection.RegloConnection"""
        port = self.xml.getElementAttribute(definition, "port")
        if port is not None:
            obj._setPort(port)
        return obj

    def write_properties(self, obj, definition):
        """Write properties from the connection object

        :type obj: pycultivator_reglo.connection.regloConnection.RegloConnection
        :type definition: lxml.etree._Element._Element
        :rtype: bool
        """
        result = super(XMLConnectionConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "port", obj.getPort())
        return result


class XMLChannelConfig(baseXMLConfig.XMLChannelConfig, baseConfig.ChannelConfig):
    """Config class to handle the channel configuration from an XML source"""

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLChannelConfig, cls).assert_source(source, expected)


class XMLPumpConfig(baseXMLConfig.XMLInstrumentConfig, baseConfig.PumpConfig):

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLPumpConfig, cls).assert_source(source, expected)


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the Light Calibration Plate XML File"""

    # set available helpers
    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLDeviceConfig,
        XMLConnectionConfig,
        XMLChannelConfig,
        XMLPumpConfig,
        baseXMLConfig.XMLCalibrationConfig,
        baseXMLConfig.XMLPolynomialConfig,
        baseXMLConfig.XMLSettingsConfig,
    ]

    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_reglo.xsd")
    XML_SCHEMA_COMPATIBILITY = {
        '1.1': 2,
        '1.0': 0,
        # version: level
        # level 2: full support
        # level 1: deprecated
        # level 0: not supported
    }

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_psimc.config.baseConfig.Config
        """
        return super(XMLConfig, cls).load(path, settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
