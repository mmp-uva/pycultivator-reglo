"""Module implementing the actual USB Communication"""

from pycultivator.connection import serialConnection
from pycultivator_reglo.connection import regloPacket, regloTemplate

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class RegloConnection (serialConnection.SerialConnection):
    """Class implementing communication with a REGLO Pump"""

    PUMP_ROTATE_CLOCKWISE = 1
    PUMP_ROTATE_COUNTERCLOCKWISE = 0
    PUMP_ROTATE_DIRECTIONS = [PUMP_ROTATE_CLOCKWISE, PUMP_ROTATE_COUNTERCLOCKWISE]

    PUMP_MODE_RPM = "L"
    PUMP_MODE_FLOWRATE = "M"
    PUMP_MODE_TIME = "N"
    PUMP_MODE_VOLUME_RATE = "O"
    PUMP_MODE_PAUSE = "]"
    PUMP_MODE_TIME_PAUSE = "P"
    PUMP_MODE_VOLUME_PAUSE = "Q"
    PUMP_MODE_VOLUME_TIME = "G"
    PUMP_MODES = [PUMP_MODE_RPM, PUMP_MODE_FLOWRATE, PUMP_MODE_TIME, PUMP_MODE_VOLUME_RATE,
                  PUMP_MODE_PAUSE, PUMP_MODE_TIME_PAUSE, PUMP_MODE_VOLUME_PAUSE, PUMP_MODE_VOLUME_TIME]

    _namespace = "pump.reglo.connection"
    _default_settings = serialConnection.SerialConnection.mergeDefaultSettings({
        "rate": 9600,
    }, namespace=_namespace)

    def __init__(self, settings=None, **kwargs):
        super(RegloConnection, self).__init__(settings=settings, **kwargs)

    def getFake(cls):
        return FakeRegloConnection

    def sendCommand(self, address, cmd, values=None):
        return self.send(
            regloPacket.RegloCommand(address, cmd, values)
        )

    def receiveShort(self, template=None, attempts=None):
        """Receive a short response

        :type template: None or pycultivator_reglo.connection.regloTemplate.RegloShortResponse
        :rtype: pycultivator_reglo.connection.regloTemplate.RegloShortResponse
        """
        if template is None:
            template = regloTemplate.RegloShortResponse()
        return self.receive(template, attempts=attempts)

    def sendAndReceiveShort(self, packet, template=None):
        return self.send(packet) and self.receiveShort(template)

    def receiveOK(self):
        """Receive a short response and check if this is an OK response

        :rtype: bool
        """
        t = self.receiveShort()
        result = t.isOk()
        if not result:
            self.getLog().warning("Expected OK (*), but received: {}".format(t.getResponse()))
        return result

    def sendAndReceiveOK(self, packet):
        return self.send(packet) and self.receiveOK()

    def receiveYes(self):
        """Receive a short response and check if this is an YES response

        :rtype: bool
        """
        t = self.receiveShort()
        result = t.isYes()
        if not result:
            self.getLog().warning("Expected YES (+), but received: {}".format(t.getResponse()))
        return result

    def receiveNo(self):
        """Receive a short response and check if this is an NO response

        :rtype: bool
        """
        t = self.receiveShort()
        result = t.isNo()
        if not result:
            self.getLog().warning("Expected NO (-), but received: {}".format(t.getResponse()))
        return result

    # Helper

    def checkAssumptions(self, **kwargs):
        self.checkConnection(**kwargs)
        self.checkAddress(**kwargs)

    def checkConnection(self, **kwargs):
        if not self.isConnected():
            raise RegloConnectionException("Not connected to pump")

    def checkAddress(self, address, lwr=1, upr=8, **kwargs):
        self.constrain(address, lwr=lwr, upr=upr, name="address", include=True)

    # Protocol

    def getCommunicationMode(self, address=0):
        """Determines the communication mode: channel address or legacy"""
        self.checkAssumptions(address=address, lwr=0)
        result = None
        if self.sendCommand(address, "~"):
            t = regloTemplate.RegloShortResponse()
            self.receive(t)
            if t.hasData():
                result = self.parseValue(t.data, int, None)
        return result

    def setCommunicationMode(self, address=0, mode=1):
        """Sets the communication mode used"""
        self.checkAssumptions(address=address, lwr=0)
        self.constrain(mode, 1, 2, name="mode", include=True)
        result = False
        if self.sendCommand(address, "~", values=[mode]):
            result = self.receiveOK()
        return result

    def getEventState(self, address=0):
        self.checkAssumptions(address=address, lwr=0)
        result = None
        if self.sendCommand(address, "xE"):
            t = regloTemplate.RegloShortResponse()
            self.receive(t)
            if t.hasData():
                result = t.data == 1
        return result

    def setEventState(self, address=0, state=False):
        self.checkAssumptions(address=address, lwr=0)
        result = False
        if self.sendCommand(address, "xE", [1 if state else 0]):
            result = self.receiveOK()
        return result

    def getProtocolVersion(self, address=0):
        """Determines the protocol version of the pump"""
        result = None
        self.checkAssumptions(address=address, lwr=0)
        if self.sendCommand(address, 'x!'):
            r = self.receiveShort()
            if r.hasData():
                result = self.parseValue(r.data, int, None)
        return result

    def getFirmware(self, address=0):
        result = None
        self.checkAssumptions(address=address, lwr=0)
        if self.sendCommand(address, "("):
            r = self.receiveShort()
            if r.hasData():
                result = self.parseValue(r.data, int, None)
        return result

    def startPump(self, address):
        """Start one channel"""
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "H"):
            result = self.receiveOK()
        return result

    def stopPump(self, address):
        """Stop one channel"""
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "I"):
            result = self.receiveOK()
        return result

    def pausePump(self, address):
        """Pause one channel. Same as STOP in RPM or Flow Rate Mode"""
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "xI"):
            result = self.receiveOK()
        return result

    def getPumpDirection(self, address):
        """Determines the direction of the pump (clockwise or counter-clockwise

        :return: -1 if counter-clockwise else 1
        :rtype: int
        """
        result = 0
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "xD"):
            r = self.receiveShort()
            if not r.hasData() or r.data not in self.PUMP_ROTATE_DIRECTIONS:
                raise RegloConnectionException(
                    "Received invalid response: {}".format(r.data)
                )
            if r.data == "K":
                result = -1
            if r.data == "J":
                result = 1
        return result

    def setPumpDirection(self, address, direction=1):
        """Sets the pump direction: 1 clockwise, <= 0 counter-clockwise"""
        result = False
        if direction == 0:
            raise RegloConnectionException(
                "Invalid direction given: {}".format(direction)
            )
        if direction > 0:
            result = self.pumpClockWise(address)
        if direction <= 0:
            result = self.pumpCounterClockWise(address)
        return result

    def pumpsClockWise(self, address):
        """Determine if the pump runs clockwise

        :return: Whether the pump runs clock wise (True == clockwise)
        :rtype: bool
        """
        return self.getPumpDirection(address) > 0

    def pumpClockWise(self, address):
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "J"):
            result = self.receiveOK()
        return result

    def pumpsCounterClockWise(self, address):
        """Determines if the pump runs counter-clockwise"""
        return self.getPumpDirection(address) < 0

    def pumpCounterClockWise(self, address):
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "K"):
            result = self.receiveOK()
        return result

    def getPumpMode(self, address):
        result = False
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "xM"):
            r = self.receiveShort()
            if not r.hasData() or r.data not in self.PUMP_MODES:
                raise RegloConnectionException(
                    "Received invalid response: {}".format(r.data)
                )
            result = r.data
        return result

    def setPumpMode(self, address, mode):
        """Sets the channel to a certain mode.

        RPM Mode = 'L'
        Flow Rate Mode = 'M'
        Volume (at rate) Mode = 'O'
        Volume (over time) Mode = 'G'
        Volume + Pause = 'Q'
        Time Mode = 'N'
        Time + Pause = 'P'

        :type address: int
        :param mode: One of the valid pump modes
        :type mode: str
        """
        self.checkAssumptions(address=address, lwr=1)
        if mode not in self.PUMP_MODES:
            raise RegloConnectionException(
                "Invalid pump mode given: {}".format(mode)
            )
        return self.sendCommand(address, mode) and self.receiveOK()

    def setPumpRPM(self, address, rpm):
        self.checkAssumptions(address=address, lwr=1)
        self.constrain(rpm, 0, 1000, name="rpm", include=True)
        p = regloPacket.RegloRPMCommand(address, "S", rpm)
        return self.sendAndReceiveOK(p)

    def getPumpRPM(self, address):
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "S"):
            t = regloTemplate.RegloFloatResponse()
            result = self.receiveShort(t).getResponse()
        return result

    def setPumpFlow(self, address, rate):
        self.checkAssumptions(address=address, lwr=1)
        self.constrain(rate, 0, name="rate", include=True)
        p = regloPacket.RegloVolumeCommand(address, "f", rate)
        return self.sendAndReceiveOK(p)

    def getPumpFlow(self, address):
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "f"):
            t = regloTemplate.RegloVolumeResponse()
            result = self.receiveShort(t).getResponse()
        return result

    def setPumpVolume(self, address, volume):
        self.checkAssumptions(address=address, lwr=1)
        self.constrain(volume, 0, name="volume")
        p = regloPacket.RegloVolumeCommand(address, "v", volume)
        return self.sendAndReceiveOK(p)

    def getPumpVolume(self, address):
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "v"):
            t = regloTemplate.RegloVolumeResponse()
            result = self.receiveShort(t).getResponse()
        return result

    def setPumpTime(self, address, time):
        """Set amount of time to pump in SECONDS"""
        self.checkAssumptions(address=address, lwr=1)
        self.constrain(time, 0, 10**7, name="time")
        time = "{:08d}".format(time*10)
        p = regloPacket.RegloCommand(address, "xT", time)
        return self.sendAndReceiveOK(p)

    def getPumpTime(self, address):
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "xT"):
            t = regloTemplate.RegloShortResponse()
            result = self.receiveShort(t).getResponse()
        return result

    def setPumpPauseTime(self, address, time):
        """Set amount of time to pause pump in SECONDS"""
        self.checkAssumptions(address=address, lwr=1)
        self.constrain(time, 0, 10**6, name="time")
        time = "{:08d}".format(time * 10)
        p = regloPacket.RegloCommand(address, "xP", time)
        return self.sendAndReceiveOK(p)

    def getPumpPauseTime(self, address):
        """Returns the pump pause time in SECONDS"""
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "xP"):
            t = regloTemplate.RegloIntResponse()
            result = self.receiveShort(t).getResponse()
        return result

    def getTubeDiameter(self, address):
        result = None
        self.checkAssumptions(address=address, lwr=1)
        if self.sendCommand(address, "+"):
            t = regloTemplate.RegloFloatResponse()
            result = self.receiveShort(t).getResponse()
        return result


class FakeRegloConnection(RegloConnection, serialConnection.FakeSerialConnection):
    """Class simulating a connection to a REGLO Pump"""

    _namespace = RegloConnection.getNameSpace()
    _default_settings = RegloConnection.mergeDefaultSettings({
        # default settings
    }, namespace=_namespace)

    def __init__(self, settings=None, **kwargs):
        super(FakeRegloConnection, self).__init__(settings=settings, **kwargs)


class RegloConnectionException(serialConnection.SerialException):
    """Exception raised by the Reglo Connection classes"""

    def __init__(self, msg):
        super(RegloConnectionException, self).__init__(msg)
