"""Module implementing Packets"""

from pycultivator.connection import Packet

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class RegloPacket(Packet.SandwichPacket):
    """Implementing a standard packet"""

    def __init__(self):
        super(RegloPacket, self).__init__()

    def getFooterFormats(self):
        return ['c']

    def getFooterValues(self):
        return [b'\r']


class RegloPumpAddressCommand(Packet.Packet):
    """Special command for setting pump address

    Structure:

    1. 0x40 ("@") - 1
    2. Addr - 1
    3. 0xd ("\r") - 1
    """

    def __init__(self, address):
        super(RegloPumpAddressCommand, self).__init__()
        self.constrain(address, 1, 8, name="address", include=True)
        self.address = address

    def getFormats(self):
        return ['B', 'c', 'B']

    def getValues(self):
        return [0x40, str(self.address), 0xd]


class RegloCommand(RegloPacket, Packet.SandwichPacket):
    """ Command Packet.

    Structure:

    1. Addr - 1
    2. Command - var
    3. Data Param1 - var (optional)
    4. 0x7c ("|") - (optional)
    5. Data Param2 - var (optiona)
    4. 0x7c ("|") - 1 (optional)
    ...
    n-2. Data Param n - var (optional)
    n-1. 0xd ("\r") - 1
    n. 0xa ("\n") - 1
    """

    def __init__(self, address, cmd, values=None):
        super(RegloCommand, self).__init__()
        self.constrain(address, 0, 4, name="address", include=True)
        if not isinstance(address, str):
            address = str(address)
        self.address = address
        if not isinstance(cmd, str):
            cmd = str(cmd)
        self.constrain(len(cmd), 1, 2, name="length of command", include=True)
        self.cmd = cmd
        self._values = []
        self.setDataValues(values)

    def getHeaderFormats(self):
        return ['c', '{}c'.format(len(self.cmd))]

    def getHeaderValues(self):
        return [self.address, self.cmd]

    def getDataFormats(self):
        return ["{}s".format(len(v)) for v in self.getDataValues()]

    def getDataValues(self):
        return self._values

    def setDataValues(self, values):
        if values is None:
            values = []
        if not isinstance(values, (tuple, list)):
            values = [values]
        self._values = [v if isinstance(v, str) else str(v) for v in values]

    def writeData(self, p=None):
        if p is None:
            p = bytearray()
        # complain if values length does not match format length
        if len(self.getDataFormats()) != len(self.getDataValues()):
            raise ValueError("Number of formats is not equal to number of values")
        for idx, fmt in enumerate(self.getDataFormats()):
            # write value
            v = self.getDataValues()[idx]
            self.writeValue(fmt, v, p)
            # also write pipe
            if idx < len(self.getDataFormats()) - 1:
                self.writeValue("c", "|", p)
        return p

    def getFooterFormats(self):
        return ['B', 'B']

    def getFooterValues(self):
        return [0xd, 0xa]


class RegloRPMCommand(RegloCommand):

    def __init__(self, address, cmd, rpm):
        """Sends a command with a RPM as parameter"""
        self.constrain(rpm, 0, 1000, name="rpm", include=True)
        # parse rate
        rpm = "{:06d}".format(rpm * 100)
        # init object
        super(RegloRPMCommand, self).__init__(address, cmd, [rpm])


class RegloVolumeCommand(RegloCommand):

    def __init__(self, address, cmd, value):
        """Sends a command with a volume as paramter"""
        self.constrain(value, 0, name="value", include=True)
        # parse value
        value = self.writeVolume(value)
        # init object
        super(RegloVolumeCommand, self).__init__(address, cmd, [value])

    @staticmethod
    def writeVolume(value):
        result, exp = "", 0
        if value >= 10:
            # need positive exponents
            while value >= 10:
                value /= 10.0
                exp += 1
        elif value < 1:
            # need negative exponents
            while value < 1:
                value *= 10.0
                exp -= 1
        result = "{:4.0f}E{:+d}".format(value * 1000, exp)
        return result
