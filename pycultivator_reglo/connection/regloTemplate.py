"""Module implementing Templates"""

from pycultivator.connection import Template

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class RegloTemplate(Template.Template):
    """Implementing a standard template"""

    def __init__(self):
        super(RegloTemplate, self).__init__()


class RegloShortResponse(RegloTemplate):
    """A template for reading short (1 character) responses"""

    def __init__(self):
        super(RegloShortResponse, self).__init__()

    RESPONSE_YES = "+"
    RESPONSE_NO = "-"
    RESPONSE_OK = "*"
    RESPONSE_ERROR = "#"

    def getFormats(self):
        return ['c']

    def isYes(self):
        """Tests if the data read by the template is an NO (+) response.

        :rtype: bool
        """
        return self.getResponse() == self.RESPONSE_YES

    def isNo(self):
        """Tests if the data read by the template is an NO (-) response.

        :rtype: bool
        """
        return self.getResponse() == self.RESPONSE_NO

    def isOk(self):
        """Tests if the data read by the template is an OK (*) response.

        :rtype: bool
        """
        return self.getResponse() == self.RESPONSE_OK

    def isError(self):
        """Tests if the data read by the template is an Error (#) response.

        :rtype: bool
        """
        return self.getResponse() == self.RESPONSE_ERROR

    def getResponse(self):
        result = None
        if self.hasData():
            result = self.data[0]
        return result

    def hasResponse(self):
        return self.hasData() and not self.isError()


class RegloIntResponse(RegloShortResponse):

    def getResponse(self):
        result = super(RegloIntResponse, self).getResponse()
        if self.hasResponse():
            result = int(result)
        return result


class RegloFloatResponse(RegloShortResponse):
    """Parses a response and automatically casts into a float"""

    def getResponse(self):
        result = super(RegloFloatResponse, self).getResponse()
        if self.hasResponse():
            result = float(result)
        return result


class RegloVolumeResponse(RegloFloatResponse):
    """Parses a response in terms of volume or flow rate in mL or mL/min"""

    def getResponse(self):
        result = super(RegloVolumeResponse, self).getResponse()
        if self.hasResponse():
            result /= 1000
        return result


class RegloStringResponse(RegloTemplate):
    """A string as response"""

    def __init__(self):
        super(RegloStringResponse, self).__init__()

    def getResponse(self):
        result = None
        if self.hasData():
            result = self.data[0]
        return result

    def __len__(self):
        return -1


class RegloEventTemplate(RegloTemplate):
    """Template for event messages"""

    def __init__(self):
        super(RegloEventTemplate, self).__init__()


class RegloResponseException(Template.TemplateException):

    def __init__(self, msg):
        super(RegloResponseException, self).__init__(msg)
