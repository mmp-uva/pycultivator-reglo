
from pycultivator_reglo.connection import regloConnection
from pycultivator_lab.script import script
from time import sleep
import argparse


class DemoScript(script.Script):

    def __init__(self):
        super(DemoScript, self).__init__()
        self._device = regloConnection.RegloConnection()
        self._port = "/dev/ttyACM0"
        self._pumps = {}

    def getPort(self):
        return self._port

    def setPort(self, port):
        self._port = port

    def hasPort(self):
        return self.getPort() is not None

    def getDevice(self):
        """

        :return:
        :rtype: pycultivator_reglo.connection.regloConnection.RegloConnection
        """
        return self._device

    def _prepare(self):
        result = False
        if self.hasPort():
            result = self.getDevice().connect(self.getPort())
        self.getLog().info("Connected to pump: {}".format(result))
        if result:
            self.configure()
        return result

    def configure(self):
        result = True
        for c in range(1, 5):
            # result = self.getDevice().setPumpMode(c, self.getDevice().PUMP_MODE_RPM) and result
            result = self.getDevice().setPumpMode(c, self.getDevice().PUMP_MODE_TIME) and result
            result = self.getDevice().setPumpRPM(c, 100) and result
            result = self.getDevice().pumpClockWise(c) and result
        return result

    def execute(self):
        self.do_pump(1, rpm=10, time=30)
        self.do_pump(2, rpm=30)
        self.do_pump(3, rpm=30, time=10)
        # try:
        #     i = 0
        #     while i < 10:
        #         sleep(10)
        # except KeyboardInterrupt:
        #     pass
        # self.do_pump(1, False)
        # self.do_pump(2, False)
        return True

    def do_pump(self, address, state=True, rpm=100, time=None):
        if state:
            if isinstance(time, (int, float)):
                mode = self.getDevice().PUMP_MODE_TIME
                self.getDevice().setPumpTime(address, time)
            else:
                mode = self.getDevice().PUMP_MODE_RPM
            result = self.getDevice().setPumpMode(address, mode)
            result = self.getDevice().setPumpRPM(address, rpm)
            result = self.getDevice().startPump(address) and result
        else:
            result = self.getDevice().stopPump(address)
        return result

    def _clean(self):
        result = True
        if self.getDevice().isConnected():
            result = self.getDevice().disconnect()
            self.getLog().info("Disconnected from pump: {}".format(result))
        result = super(DemoScript, self)._clean() and result
        return result

    def default_arguments(self):
        defaults = super(DemoScript, self).default_arguments()
        defaults["port"] = self.getPort()
        return defaults

    def define_arguments(self, parser=None):
        if parser is None:
            parser = argparse.ArgumentParser(description="Demo application of Reglo Pump")
        parser = super(DemoScript, self).define_arguments(parser)
        parser.add_argument(
            "--port", help="Set port to which the pump is connected"
        )
        return parser

    def parse_arguments(self, arguments):
        super(DemoScript, self).parse_arguments(arguments)
        self.setPort(arguments.get("port", self.getPort()))

if __name__ == "__main__":
    ds = DemoScript()
    # create argument parser
    p = ds.define_arguments()
    # load defaults
    a = ds.default_arguments()
    # load arguments and update defaults
    a.update(vars(p.parse_args()))
    # parse arguments
    ds.parse_arguments(a)
    # run script
    ds.run()
