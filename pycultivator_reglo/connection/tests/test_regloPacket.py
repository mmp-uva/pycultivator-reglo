"""Module for unittesting regloPacket in the connection package"""

from pycultivator.foundation.tests import _test
from pycultivator_reglo.connection import regloPacket

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class RegloPacketTest(_test.UvaSubjectTestCase):
    """Tests the base reglo packet"""

    _abstract = False
    _subject_cls = regloPacket.RegloPacket

    def test_write(self):
        self.assertEqual(
            self.getSubject().write(), bytearray("\r")
        )


class RegloPumpAddressCommandTest(RegloPacketTest):

    _abstract = False
    _subject_cls = regloPacket.RegloPumpAddressCommand

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(1)

    def test_init(self):
        with self.assertRaises(ValueError):
            self.getSubjectClass()(0)
        with self.assertRaises(ValueError):
            self.getSubjectClass()(9)

    def test_write(self):
        self.assertEqual(
            self.getSubject().write(), bytearray('@1\r')
        )


class RegloCommandTest(RegloPacketTest):

    _abstract = False
    _subject_cls = regloPacket.RegloCommand

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(1, "~")

    def test_init(self):
        with self.assertRaises(ValueError):
            self.getSubjectClass()(-1, "~")
        with self.assertRaises(ValueError):
            self.getSubjectClass()(9, "~")
        with self.assertRaises(ValueError):
            self.getSubjectClass()(1, "")
        with self.assertRaises(ValueError):
            self.getSubjectClass()(1, "abc")
        p = self.getSubjectClass()(1, "ab", [1, "a", 3])
        self.assertEqual(p.getData(), ['1', "ab", "1", "a", "3", 13, 10])
        self.assertEqual(p.getDataValues(), ["1", "a", "3"])

    def test_setDataValues(self):
        self.getSubject().setDataValues([])
        self.assertEqual(self.getSubject().getDataValues(), [])
        self.getSubject().setDataValues([1, "a", 3])
        self.assertEqual(self.getSubject().getDataValues(), ["1", "a", "3"])

    def test_getDataFormats(self):
        self.getSubject().setDataValues([1, "aa", 3])
        self.assertEqual(self.getSubject().getDataFormats(), ["1s", "2s", "1s"])

    def test_write(self):
        self.assertEqual(
            self.getSubject().write(), bytearray("1~\r\n")
        )
        self.getSubject().setDataValues([1, "aa", 3])
        self.assertEqual(
            self.getSubject().write(), bytearray("1~1|aa|3\r\n")
        )