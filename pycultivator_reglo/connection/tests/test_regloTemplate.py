"""Module for unittesting regloPacket in the connection package"""

from pycultivator.foundation.tests import _test
from pycultivator_reglo.connection import regloTemplate

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class RegloTemplateTest(_test.UvaSubjectTestCase):
    """Tests the base reglo packet"""

    _abstract = False
    _subject_cls = regloTemplate.RegloTemplate

    def test_write(self):
        self.assertEqual(
            self.getSubject().write(), bytearray("\r")
        )
