"""The PSI Multi-Cultivator provides bindings to Ismatec's REGLO ICC Pump"""

from pycultivator.device import device, channel
from pycultivator_reglo import pump

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PumpDevice(device.ComplexDevice):
    """The REGLO ICC Pump"""

    _namespace = "psimc"

    _default_settings = device.Device.mergeDefaultSettings(
        {

        }, namespace=_namespace
    )

    MAX_CHANNELS = 4

    def __init__(self, name, parent, settings=None, **kwargs):
        super(PumpDevice, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_psimc.connection.mcConnection.MCConnection
        """
        return super(PumpDevice, self).getConnection()

    def createConnection(self, fake=False, **kwargs):
        """Creates a new connection and uses it for this device"""
        from pycultivator_reglo.connection import regloConnection
        if not fake:
            self.setConnection(regloConnection.RegloConnection(**kwargs))
        else:
            self.setConnection(regloConnection.FakeRegloConnection(**kwargs))
        return self.getConnection()

    def getChannels(self):
        """ Returns all the wells known by this device

        :rtype: dict[int, pycultivator_psimc.device.MultiCultivator.MultiCultivatorChannel]
        """
        return super(PumpDevice, self).getChannels()

    def getChannel(self, idx):
        """ Returns the well at the given index

        :rtype: pycultivator_psimc.device.MultiCultivator.MultiCultivatorChannel
        """
        return super(PumpDevice, self).getChannel(idx)

    # Behaviour methods

    def connect(self, fake=False):
        """Connect to the Light Calibration Plate"""
        result = False
        if not self.hasConnection():
            self.createConnection(fake=fake)
        if self.hasConnection():
            result = self.getConnection().connect()
        return result

    # def control(self, instrument, variable, value, **kwargs):
    #     if instrument == "light":
    #         results = self.controLight(variable=variable, value=variable, **kwargs)
    #     else:
    #         results = super(MultiCultivator, self).control(instrument, variable, value, **kwargs)
    #     return results


class PumpChannel(channel.Channel):
    """A channel in a Reglo ICC Pump"""

    _namespace = "mc.channel"

    _default_settings = channel.Channel.mergeDefaultSettings(
        {

        }, namespace=_namespace
    )

    INSTRUMENT_PUMP = "pump"

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PumpChannel, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)

    # Getters // Setters

    def getDevice(self):
        """ Returns the Multi-Cultivator object to which this channel belongs.

        :return: The plate object
        :rtype: pycultivator_reglo.device.regloPump.RegloPump
        """
        return super(PumpChannel, self).getDevice()

    def addInstrument(self, i):
        """Registers an instrument under it's own name to the channel.

        Only allows 1 Light Panel and 1 OD Sensor to be registered to this channel

        :type i: pycultivator_reglo.instrument.pump.Pump
        """
        name = i.getName()
        if not isinstance(i, pump.Pump):
            raise RegloPumpException(
                "A REGLO ICC Pump can only contain Pump Instruments. Not - {}".format(type(i))
            )
        if self.countInstrumentsOfType("pump") == 4:
            raise RegloPumpException(
                "A REGLO ICC Pump already contains 4 Pumps, cannot add new"
            )
        if name is not None:
            self.getInstruments()[name] = i
            i.setParent(self)
        return self


class RegloPumpException(device.DeviceException):
    """An exception raised by the LightCalibrationPlate class"""

    pass
