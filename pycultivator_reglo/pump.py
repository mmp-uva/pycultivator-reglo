"""Module implementing a Pump Instrument"""

from pycultivator.instrument import Instrument, InstrumentVariable, InstrumentException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Pump(Instrument):
    """A pump instrument class"""

    _rate = InstrumentVariable(
        None, private=True
    )
    _direction = InstrumentVariable(
        None, private=True
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Pump, self).__init__(identity, parent, settings=settings, **kwargs)

    def _measure(self, source, **kwargs):
        pass

class PumpException(InstrumentException):
    """Exception raised by a Pump Instrument"""

    def __init__(self, msg):
        super(PumpException, self).__init__(msg)