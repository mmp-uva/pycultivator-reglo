"""A global test script to test the functioning of the whole package"""

import _test

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PackageTestCase(_test.LiveRegloTestCase):
    """Tests the package"""

    def test_instruments(self):
        self.skipTest("Not implemented yet")

    def test_controls(self):
        self.skipTest("Not implemented yet")

    def test_control(self):
        self.skipTest("Not implemented yet")

    def test_measures(self):
        self.skipTest("Not implemented yet")

    def test_measure(self):
        self.skipTest("Not implemented yet")
